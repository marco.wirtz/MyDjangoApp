# -*- coding: utf-8 -*-

"""

ECTOPLANNER SOFTWARE

Developed by:  E.ON Energy Research Center, 
               Institute for Energy Efficient Buildings and Indoor Climate, 
               RWTH Aachen University, 
               Germany

Author:        Marco Wirtz

Development period: 2018/2019

"""

from django.shortcuts import render
#from django.http import HttpResponse
#from django.template import loader
#from .forms import UploadFileForm
#from django.views.static import serve
#from django.core.files.storage import FileSystemStorage
#from django.conf import settings
from myapp.help_functions import intra_balancing, design_balancing_unit, load_params, collect_results


def index(request):
    return render(request, "myapp/index.html", {})


def calculation(request):
    
    if request.method == "POST":
	
		# SET BASIC PARAMETER
        time_steps 			= range(8760)

		# EXTRACT INPUT PARAMETER AND LOAD FURTHER PARAMETER
        param, devs, nodes = load_params.load_params(request)
		
		# INTRA-BUILDING BALANCING
        nodes = intra_balancing.calc_residuals(nodes, param, time_steps)
		
		# INTER-BUILDING BALANCING and DESIGN of BALANCING UNIT
        res_bal_unit, res_subsidies, optimization_successful = design_balancing_unit.design_balancing_unit(nodes, devs, param, time_steps)

        if optimization_successful:
            
            # CALCULATE AND COLLECT RESULTS FOR RENDERING
            main_results, res_intra_bldg_bal, res_bldg_bal, nodes, res_nodes  = collect_results.collect_results(nodes, param, devs, res_bal_unit, time_steps)
    
            result_dict = {"main_results": main_results,
                           "res_intra_bldg_bal": res_intra_bldg_bal,
                           "res_bal_unit": res_bal_unit,
                           "res_bldg_bal": res_bldg_bal,
                           "res_subsidies": res_subsidies,
                           "res_nodes": res_nodes, # contains rounded data from nodes dictionary
    					   }
        
            result_dict["processing_successful"] = True
        
            print("Processing successful, next step: HTML-Rendering")
        
            return render(request, "myapp/calc.html", result_dict)
    
        else:
            
            result_dict = {"processing_successful": False,
                           "optimization_successful": optimization_successful,
                           }
        
            print("Processing NOT successful due to failed optimization, next step: HTML-Rendering")
        
            return render(request, "myapp/calc.html", result_dict)
            
        
    else:
        
        result_dict = {"processing_successful": False,
                       "optimization_successful": True,
                       }
        
        return render(request, "myapp/calc.html", result_dict)
        
