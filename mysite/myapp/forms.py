from django import forms

class UploadFileForm(forms.Form):
    title = forms.CharField(max_length=50, label="Project name")
    file = forms.FileField()
