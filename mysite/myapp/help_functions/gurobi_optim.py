# -*- coding: utf-8 -*-

"""

ECTOPLANNER SOFTWARE

Developed by:  E.ON Energy Research Center, 
               Institute for Energy Efficient Buildings and Indoor Climate, 
               RWTH Aachen University, 
               Germany

Author:        Marco Wirtz

Development period: 2018/2019

"""

import gurobipy as gp
import time

def run_optim(devs, param, residual, time_steps):
    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # Load model parameter
    start_time = time.time()

    # Create set for devices
    all_devs = ["BOI", "EH", "GSHP", "CHP", "AC", "CC", "TES", "CTES", "BAT", "from_DH", "from_DC", "from_WASTE", "from_WASTE_cold", "PV"]       

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # Setting up the model
    
    # Create a new model
    model = gp.Model("Balancing_Unit_Model")
    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # Create new variables

    # Purchase decision binary variables (1 if device is installed, 0 otherwise)
    x = {}
    for device in all_devs:
        x[device] = model.addVar(vtype="B", name="x_" + str(device))
            
    # Device's capacity (i.e. nominal power)
    cap = {}
    for device in ["BOI", "EH", "GSHP", "CHP", "AC", "CC", "TES", "CTES", "BAT", "from_DH", "from_DC", "from_WASTE", "from_WASTE_cold", "PV"]:
        cap[device] = model.addVar(vtype="C", name="nominal_capacity_" + str(device))
    
    # Gas flow to/from devices
    gas = {}
    for device in ["BOI", "CHP"]:
        gas[device] = {}
        for t in time_steps:
            gas[device][t] = model.addVar(vtype="C", name="gas_" + device + "_t" + str(t))
        
    # Eletrical power to/from devices
    power = {}
    for device in ["CHP", "EH", "GSHP", "CC", "from_grid", "to_grid", "PV"]:
        power[device] = {}
        for t in time_steps:
            power[device][t] = model.addVar(vtype="C", name="power_" + device + "_t" + str(t))
       
    # Heat to/from devices
    heat = {}
    for device in ["BOI", "EH", "GSHP", "CHP", "AC", "from_DH", "from_WASTE"]:
        heat[device] = {}
        for t in time_steps:
            heat[device][t] = model.addVar(vtype="C", name="heat_" + device + "_t" + str(t))
    
    # Cooling power to/from devices
    cool = {}
    for device in ["CC", "AC", "from_DC", "from_WASTE_cold"]:
        cool[device] = {}
        for t in time_steps:
            cool[device][t] = model.addVar(vtype="C", name="cool_" + device + "_t" + str(t))

    # Storage decision variables
    ch = {}  # Energy flow to charge storage device
    dch = {} # Energy flow to discharge storage device
    soc = {} # State of charge

    for device in ["TES", "CTES", "BAT"]:
        ch[device] = {}
        dch[device] = {}
        soc[device] = {}
        for t in time_steps:
            ch[device][t] = model.addVar(vtype="C", name="ch_" + device + "_t" + str(t))
            dch[device][t] = model.addVar(vtype="C", name="dch_" + device + "_t" + str(t))
            soc[device][t] = model.addVar(vtype="C", name="soc_" + device + "_t" + str(t))
        soc[device][len(time_steps)] = model.addVar(vtype="C", name="soc_" + device + "_t" + str(len(time_steps)))

    # Objective function
    obj = {}
    set_obj = ["tac"]
    for k in set_obj:
        obj[k] = model.addVar(vtype="C", lb=-gp.GRB.INFINITY, name="obj_" + k)    

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # Define objective function
    model.update()
    model.setObjective(obj["tac"], gp.GRB.MINIMIZE)

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # Add constraints
    
    # Constraints defined by user in GUI
    for device in ["TES", "BAT", "CTES", "BOI", "from_DH", "EH", "GSHP", "CHP", "CC", "AC", "from_DC", "from_WASTE", "from_WASTE_cold", "PV"]:
        if devs[device]["feasible"] == "True":
            model.addConstr(x[device] == 1)
        else:
            model.addConstr(x[device] == 0)
        
    
    #%% CONTINUOUS SIZING OF DEVICES: minimum capacity <= capacity <= maximum capacity
    for device in ["BOI", "CHP", "EH", "GSHP", "CC", "AC", "TES", "CTES", "BAT", "PV"]:
        model.addConstr(cap[device] <= x[device] * devs[device]["max_cap"])
        
    for device in ["BOI", "CHP", "EH", "GSHP", "CC", "AC", "TES", "CTES", "BAT", "PV"]:
        model.addConstr(cap[device] >= x[device] * devs[device]["min_cap"])
    
    for t in time_steps:
        for device in ["BOI", "GSHP", "from_DH", "EH"]:
            model.addConstr(heat[device][t] <= x[device] * cap[device])
            
        for device in ["CHP", "PV"]:
            model.addConstr(power[device][t] <= x[device] * cap[device])
                
        for device in ["CC", "AC", "from_DC"]:
            model.addConstr(cool[device][t] <= x[device] * cap[device])
        
        # Waste heat
        model.addConstr(heat["from_WASTE"][t] <= x["from_WASTE"] * cap["from_WASTE"])
        model.addConstr(cool["from_WASTE_cold"][t] <= x["from_WASTE_cold"] * cap["from_WASTE_cold"])
        
    model.addConstr(cap["from_WASTE"] == devs["from_WASTE"]["waste_heat_power"])
    model.addConstr(cap["from_WASTE_cold"] == devs["from_WASTE_cold"]["waste_cooling_power"])


    #%% INPUT / OUTPUT CONSTRAINTS
    for t in time_steps:
        # Boiler
        model.addConstr(gas["BOI"][t] == heat["BOI"][t] / devs["BOI"]["eta_th"])
        
        # Combined heat and power
        model.addConstr(power["CHP"][t] == heat["CHP"][t] / devs["CHP"]["eta_th"] * devs["CHP"]["eta_el"])
        model.addConstr(gas["CHP"][t] == heat["CHP"][t] / devs["CHP"]["eta_th"])
        
        # Electric heater
        model.addConstr(heat["EH"][t] == power["EH"][t] * devs["EH"]["eta_th"])
        
        # Ground source heat pump
        model.addConstr(heat["GSHP"][t] == power["GSHP"][t] * devs["GSHP"]["COP"])
        
        # Compression chiller
        model.addConstr(cool["CC"][t] == power["CC"][t] * devs["CC"]["COP"])  

        # Absorption chiller
        model.addConstr(cool["AC"][t] == heat["AC"][t] * devs["AC"]["eta_th"])
        
        # PV
        model.addConstr(power["PV"][t] <= devs["PV"]["PV_power_norm"][t] * cap["PV"])

    #%% ENERGY BALANCES
    for t in time_steps:
        # Heat balance
        model.addConstr(heat["BOI"][t] + heat["EH"][t] + heat["GSHP"][t] + heat["CHP"][t] + heat["from_DH"][t] + heat["from_WASTE"][t] + dch["TES"][t] == residual["heat"][t] + heat["AC"][t] + ch["TES"][t])
        model.addConstr(heat["BOI"][t] + heat["EH"][t] + heat["CHP"][t] >= heat["AC"][t] + ch["TES"][t])

    for t in time_steps:
        # Electricity balance
        model.addConstr(power["CHP"][t] + dch["BAT"][t] + power["PV"][t] + power["from_grid"][t] == residual["power"][t] + power["to_grid"][t] + power["CC"][t] + power["EH"][t] + power["GSHP"][t] + ch["BAT"][t])

    for t in time_steps:
        # Cooling balance
        model.addConstr(cool["AC"][t] + cool["CC"][t] + cool["from_DC"][t] + cool["from_WASTE_cold"][t] + dch["CTES"][t] == residual["cool"][t] + ch["CTES"][t])    
    
    #%% STORAGE DEVICES
    for device in ["TES", "CTES", "BAT"]:
        # Cyclic condition
        model.addConstr(soc[device][len(time_steps)] == soc[device][0])

        for t in range(len(time_steps)+1):
            if t == 0:
                pass
                # Set initial state of charge
                model.addConstr(soc[device][0] <= cap[device] * devs[device]["soc_init"])
            else:
                # Energy balance: soc(t) = soc(t-1) + charge - discharge
                model.addConstr(soc[device][t] == soc[device][t-1] * (1-devs[device]["sto_loss"])
                    + (ch[device][t-1] * devs[device]["eta_ch"] 
                    - dch[device][t-1] / devs[device]["eta_dch"]))
                
                # soc_min <= state of charge <= soc_max
                model.addConstr(soc[device][t] <= cap[device])

    #%% SUM UP RESULTS
    gas_total = sum(sum(gas[device][t] for t in time_steps) for device in ["BOI", "CHP"])
    from_grid_total = sum(power["from_grid"][t] for t in time_steps)
    to_grid_total = sum(power["to_grid"][t] for t in time_steps)
    from_DH_total = sum(heat["from_DH"][t] for t in time_steps)
    from_DC_total = sum(cool["from_DC"][t] for t in time_steps)
    from_waste_total = sum(heat["from_WASTE"][t] for t in time_steps)
    from_waste_cool_total = sum(cool["from_WASTE_cold"][t] for t in time_steps)
    
    # Forbid/allow feed in depending on user input:
    if param["feasible_feed_in"] != "True":
        model.addConstr(to_grid_total == 0)
    
    # Investments
    c_inv = {}
    for device in all_devs:
        c_inv[device] = cap[device] * devs[device]["ann_inv_var"]

    # Operation and maintenance costs
    c_om = {}
    for device in all_devs: 
        c_om[device] = devs[device]["cost_om"] * (cap[device] * devs[device]["inv_var"])

    #%% OBJECTIVE FUNCTIONS
    # TOTAL ANNUALIZED COSTS
    model.addConstr(obj["tac"] == sum(c_inv[dev] for dev in all_devs) 
                                  + sum(c_om[dev] for dev in all_devs) 
                                  + gas_total * param["price_gas"] 
                                  + from_grid_total * param["price_el"] 
                                  - to_grid_total * param["revenue_feed_in"]
                                  + from_DC_total * devs["from_DC"]["price_DC"] 
                                  + from_DH_total * devs["from_DH"]["price_DH"]
                                  + from_waste_total * devs["from_WASTE"]["price_waste_heat"]
                                  + from_waste_cool_total * devs["from_WASTE_cold"]["price_waste_cold"], "sum_up_TAC")

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # Set model parameters and execute calculation
    
    print("Precalculation and model set up done in %f seconds." %(time.time() - start_time))
    
    # Set solver parameters
    model.Params.MIPGap     = 0.02   # ---,         gap for branch-and-bound algorithm
    model.Params.method     = 2      # ---,         -1: default, 0: primal simplex, 1: dual simplex, 2: barrier, etc.
    model.Params.MIPFocus   = 2
    
    # Execute calculation
    start_time = time.time()

    model.optimize()

    print("Optimization done. (%f seconds.)" %(time.time() - start_time))
    
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # Check and save results
    
    # Check if optimal solution was found
    if model.Status in (3,4) or model.SolCount == 0:  # "INFEASIBLE" or "INF_OR_UNBD"
        optimization_successful = False
        empty_set = {}
#        model.computeIIS()
#        model.write("model.ilp")
        print('Optimization result: No feasible solution found.') 
        return empty_set, empty_set, optimization_successful
        
    else:
        optimization_successful = True
		
        res_bal_unit = {}
        res_bal_unit["obj"] = {}        
        for k in set_obj:
            res_bal_unit["obj"][k] = round(obj[k].x,2)
		
        res_bal_unit["cap"] = {}        
        for k in cap.keys():
            res_bal_unit["cap"][k] = round(cap[k].x,1)
			
        res_bal_unit["ann_total_bu_equip_cost"] = round(sum(cap[k].X * devs[k]["ann_inv_var"] for k in cap.keys()),0)
        res_bal_unit["total_bu_equip_cost"] = round(sum(cap[k].X * devs[k]["inv_var"] for k in cap.keys()),0)
        res_bal_unit["total_bu_om_cost"] = round(sum(devs[k]["cost_om"] * cap[k].X * devs[k]["inv_var"] for k in cap.keys()),0)     
        res_bal_unit["total_from_grid"] = round(sum(power["from_grid"][t].X for t in time_steps),1)
        res_bal_unit["total_to_grid"] = round(sum(power["to_grid"][t].X for t in time_steps),1)
        res_bal_unit["total_from_DH"] = round(sum(heat["from_DH"][t].X for t in time_steps),1)
        res_bal_unit["total_from_DC"] = round(sum(cool["from_DC"][t].X for t in time_steps),1)
        res_bal_unit["total_from_WASTE"] = round(sum(heat["from_WASTE"][t].X for t in time_steps),1)
        res_bal_unit["total_from_WASTE_cold"] = round(sum(cool["from_WASTE_cold"][t].X for t in time_steps),1)
        res_bal_unit["total_gas"] = round(sum(gas["BOI"][t].X + gas["CHP"][t].X for t in time_steps),1)
        res_bal_unit["total_PV_power"] = round(sum(power["PV"][t].X for t in time_steps),1)
        res_bal_unit["total_PV_area"] = round(cap["PV"].X / (devs["PV"]["nom_module_power"] / 1000) * devs["PV"]["module_area"],0)
        res_bal_unit["total_PV_power_curtailed"] = round(sum(devs["PV"]["PV_power_norm"][t] for t in time_steps) * cap["PV"].X - sum(power["PV"][t].X for t in time_steps),1)
        
        # Write Gurobi object data to dictionary
        dec_var = {}
        dec_var["power"] = {"PV": {},
                            "CHP": {},
                            "to_grid": {},
                            "from_grid": {},
                            }
        for k in dec_var["power"].keys():
            for t in time_steps:
                dec_var["power"][k][t] = power[k][t].X
            
        dec_var["heat"] = {"CHP": {}}
        for k in dec_var["heat"].keys():
            for t in time_steps:
                dec_var["heat"][k][t] = heat[k][t].X
        
        dec_var["ch"] = {"TES": {}}
        for k in dec_var["ch"].keys():
            for t in time_steps:
                dec_var["ch"][k][t] = ch[k][t].X
        
        dec_var["cap"] = {"CHP": cap["CHP"].X,
                          "TES": cap["TES"].X,
                          }
        
        return res_bal_unit, dec_var, optimization_successful