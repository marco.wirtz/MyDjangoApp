# -*- coding: utf-8 -*-

"""

ECTOPLANNER SOFTWARE

Developed by:  E.ON Energy Research Center, 
               Institute for Energy Efficient Buildings and Indoor Climate, 
               RWTH Aachen University, 
               Germany

Author:        Marco Wirtz

Development period: 2018/2019

"""

import numpy as np

def calc_residuals(nodes, param, time_steps):
    
    for n in range(len(nodes)):
        print("MAX: " + str(np.max(nodes[n]["cool"])))
        
        power = {"HP": np.zeros(len(time_steps)), 
                 "CC": np.zeros(len(time_steps)),
                 "EH": np.zeros(len(time_steps)),
                 }
        heat = {"HP": np.zeros(len(time_steps))}
        gas = {"BOI": np.zeros(len(time_steps))}
        
        # Determine capacity of heat pump, electric heater, boiler
        if (param["use_eh_in_bldgs"] == False and param["use_boi_in_bldgs"] == False):
            hp_capacity = np.max(nodes[n]["heat"])
            eh_capacity = 0
            boi_capacity = 0
            
        else:
            for t in time_steps:
                if param["air_temp"][t] > param["bivalent_temp"]:
                    heat["HP"][t] = nodes[n]["heat"][t]                    
                    
            hp_capacity = np.max(heat["HP"])
            if param["use_eh_in_bldgs"] == True:
                eh_capacity = np.max(nodes[n]["heat"]) - hp_capacity
                boi_capacity = 0
            elif param["use_boi_in_bldgs"] == True:
                eh_capacity = 0
                boi_capacity = np.max(nodes[n]["heat"]) - hp_capacity
                
        # Determine capacity of chiller
        if param["enable_free_cooling"] == False:
            cc_capacity = np.max(nodes[n]["cool"])
            frc_capacity = 0     
        else:
            cc_capacity = 0
            frc_capacity = np.max(nodes[n]["cool"])
        
        BES_heat_dem = np.zeros(len(time_steps)) # heating demand of building energy system (heat pump)
        BES_cool_dem = np.zeros(len(time_steps)) # cooling demand of building energy system (chiller, free cooler)
        
        # Determine operation in each time step
        for t in time_steps:
            if nodes[n]["heat"][t] <= hp_capacity:
                power["HP"][t] = nodes[n]["heat"][t] / param["COP_HP"][t]
                power["EH"][t] = 0
                gas["BOI"][t] = 0
            else:
                power["HP"][t] = hp_capacity / param["COP_HP"][t]
                if param["use_eh_in_bldgs"] == True:
                    power["EH"][t] = (nodes[n]["heat"][t] - hp_capacity) / param["eta_th_eh_bldg"]
                    gas["BOI"][t] = 0
                elif param["use_boi_in_bldgs"] == True:    
                    power["EH"][t] = 0
                    gas["BOI"][t] = (nodes[n]["heat"][t] - boi_capacity) / param["eta_th_boi_bldg"]
                
            BES_heat_dem[t] = power["HP"][t] * (param["COP_HP"][t] - 1)
            if param["enable_free_cooling"] == False:
                power["CC"][t] = nodes[n]["cool"][t] / param["COP_CC"][t]
                BES_cool_dem[t] = power["CC"][t] + nodes[n]["cool"][t]
                
            else:
                power["CC"][t] = 0
                BES_cool_dem[t] = nodes[n]["cool"][t]
        
                
        # Write results to node dictionary
        nodes[n]["hp_capacity"] =   hp_capacity # kW_th
        nodes[n]["hp_costs"] =      hp_capacity * param["inv_var_hp_bldgs"]
        nodes[n]["hp_ann_costs"] =  hp_capacity * param["ann_inv_var_hp_bldgs"] 
        
        nodes[n]["eh_capacity"] =   eh_capacity # kW_th
        nodes[n]["eh_costs"] =      eh_capacity * param["inv_var_eh_bldgs"]
        nodes[n]["eh_ann_costs"] =  eh_capacity * param["ann_inv_var_eh_bldgs"]
        
        nodes[n]["boi_capacity"] =  boi_capacity # kW_th
        nodes[n]["boi_costs"] =     boi_capacity * param["inv_var_boi_bldgs"]
        nodes[n]["boi_ann_costs"] = boi_capacity * param["ann_inv_var_boi_bldgs"]
        
        nodes[n]["cc_capacity"] =   cc_capacity # kW_th
        nodes[n]["cc_costs"] =      cc_capacity * param["inv_var_cc_bldgs"]
        nodes[n]["cc_ann_costs"] =  cc_capacity * param["ann_inv_var_cc_bldgs"]
        
        nodes[n]["frc_capacity"] =  frc_capacity # kW_th
        nodes[n]["frc_costs"] =     frc_capacity * param["inv_var_frc_bldgs"]
        nodes[n]["frc_ann_costs"] = frc_capacity * param["ann_inv_var_frc_bldgs"]
        
        nodes[n]["BES_heat_dem"] = BES_heat_dem # kW_th
        nodes[n]["BES_cool_dem"] = BES_cool_dem # kW_th
        nodes[n]["res_heat_dem"] = BES_heat_dem - BES_cool_dem # kW_th
        
        nodes[n]["power_HP"] = power["HP"]
        nodes[n]["power_EH"] = power["EH"]
        nodes[n]["power_CC"] = power["CC"]
        nodes[n]["gas_BOI"] = gas["BOI"]
        nodes[n]["total_power_dem"] = round(sum(power["HP"][t] + power["EH"][t] + power["CC"][t] for t in time_steps),0)
        nodes[n]["total_gas_dem"] = round(sum(gas["BOI"][t] for t in time_steps),0)

    return nodes