# -*- coding: utf-8 -*-

"""

ECTOPLANNER SOFTWARE

Developed by:  E.ON Energy Research Center, 
               Institute for Energy Efficient Buildings and Indoor Climate, 
               RWTH Aachen University, 
               Germany

Author:        Marco Wirtz

Development period: 2018/2019

"""

import numpy as np
import math

def load_params(request):

    param = {}
	
    path_input_data = "myapp/input_data/"
	
	################################################################
	# LOAD WEATHER DATA
    param["weather_location"] = request.POST["weather_location"]
        
    param["air_temp"] = np.loadtxt(path_input_data + "/air_temp/air_temp_" + param["weather_location"] + ".txt")
    param["GHI"] = np.loadtxt(path_input_data + "/GHI/GHI_" + param["weather_location"] + ".txt")
        
	################################################################
	# LOAD BUILDING DATA
    nodes = {}
		
    # Determine number of buildings
    param_list = request.POST.dict()
    for param_entry in param_list.keys():
        if "bldg_" in param_entry:
            param_split = param_entry.split("_")
            if not int(param_split[1]) in nodes.keys():
                nodes[int(param_split[1])] = {}
                nodes[int(param_split[1])]["name"] = "bldg_" + str(int(param_split[1]))
                nodes[int(param_split[1])]["id"] = int(param_split[1])+1
	
    
    # calculate normalized profile
    norm_heat_dem_residential, norm_cool_dem_residential = calc_norm_demands(param["air_temp"], 15, 15)
			
    # calculate normalized profile
    norm_heat_dem_office, norm_cool_dem_office = calc_norm_demands(param["air_temp"], 15, 12)
            
    # Load/calculate demand time series
    for n in nodes.keys():
		
        # if two files were uploaded for the building, use this data
        if str("bldg_" + str(n) + "_file_heat_dem") in request.FILES and str("bldg_" + str(n) + "_file_cool_dem") in request.FILES:
            nodes[n]["heat"] = np.loadtxt(request.FILES["bldg_" + str(n) + "_file_heat_dem"])
            nodes[n]["cool"] = np.loadtxt(request.FILES["bldg_" + str(n) + "_file_cool_dem"])
		
		# if only heat demand file uploaded, use this data
        elif str("bldg_" + str(n) + "_file_heat_dem") in request.FILES:
            nodes[n]["heat"] = np.loadtxt(request.FILES["bldg_" + str(n) + "_file_heat_dem"])
            nodes[n]["cool"] = np.zeros(8760)
		
        # if only cooling demand file uploaded, use this data
        elif str("bldg_" + str(n) + "_file_cool_dem") in request.FILES:
            nodes[n]["heat"] = np.zeros(8760)
            nodes[n]["cool"] = np.loadtxt(request.FILES["bldg_" + str(n) + "_file_cool_dem"])
		
		# if no file was uploaded, take data for type buildings
        else:
            heated_cooled_area = float(request.POST["bldg_" + str(n) + "_heated_area"])
            spec_heat_dem = float(request.POST["bldg_" + str(n) + "_spec_heat_dem"])
            spec_cool_dem = float(request.POST["bldg_" + str(n) + "_spec_cool_dem"])
            bldg_type = request.POST["bldg_" + str(n) + "_type"]
            
            if bldg_type == "residential":
                # calculate heating time series
                nodes[n]["heat"] = norm_heat_dem_residential * heated_cooled_area * spec_heat_dem
                nodes[n]["cool"] = norm_cool_dem_residential * heated_cooled_area * spec_cool_dem

            else:
                # calculate heating time series
                nodes[n]["heat"] = norm_heat_dem_office * heated_cooled_area * spec_heat_dem
                nodes[n]["cool"] = norm_cool_dem_office * heated_cooled_area * spec_cool_dem
                
                
	################################################################
	# LOAD TECHNICAL PARAMETERS
	
	# Building energy system
    param["bivalent_temp"] = float(request.POST["bivalent_temp"])
    
    if "enable_free_cooling" in request.POST:
        param["enable_free_cooling"] = True
    else:
        param["enable_free_cooling"] = False
       
    if "use_eh_in_bldgs" in request.POST:
        param["use_eh_in_bldgs"] = True
    else:
        param["use_eh_in_bldgs"] = False
    param["eta_th_eh_bldg"] = float(request.POST["eta_th_eh_bldg"])
	
    if "use_boi_in_bldgs" in request.POST:
        param["use_boi_in_bldgs"] = True
    else:
        param["use_boi_in_bldgs"] = False
        
    param["eta_th_boi_bldg"] = float(request.POST["eta_th_boi_bldg"])
    param["inv_var_hp_bldgs"] = float(request.POST["inv_var_hp_bldgs"])
    param["inv_var_cc_bldgs"] = float(request.POST["inv_var_cc_bldgs"])
    param["inv_var_eh_bldgs"] = float(request.POST["inv_var_eh_bldgs"])
    param["inv_var_boi_bldgs"] = float(request.POST["inv_var_boi_bldgs"])
    param["inv_var_frc_bldgs"] = 0
    
    if "enable_use_carnot_cops" in request.POST:
        param["enable_use_carnot_cops"] = True
    else:
        param["enable_use_carnot_cops"] = False

    if "enable_use_fix_cops" in request.POST:
        param["enable_use_fix_cops"] = True
    else:
        param["enable_use_fix_cops"] = False	
		
    param["supply_temperature_heating"] = float(request.POST["supply_temperature_heating"])
    param["supply_temperature_cooling"] = float(request.POST["supply_temperature_cooling"])
    param["carnot_efficiency"] = float(request.POST["carnot_efficiency"])
    param["COP_heat_pumps"] = float(request.POST["COP_heat_pumps"])
    param["COP_chillers"] = float(request.POST["COP_chillers"])
    
	
	# Network data
    param["network_temp_hot"] = float(request.POST["network_temp_hot"])
    param["network_temp_cold"] = float(request.POST["network_temp_cold"])
    param["network_length"] = float(request.POST["network_length"])
	
	# Waste heat
    devs = {}
    devs["from_WASTE"] = {"feasible": "False",
                          "waste_heat_temp": float(request.POST["waste_heat_temp"]),
                          "waste_heat_power": float(request.POST["waste_heat_power"]),
                          "price_waste_heat": float(request.POST["price_waste_heat"]),
                          "inv_var": 0,
				          "life_time": 20,
				          "cost_om": 0,
                         }

    if "feasible_waste_heat" in request.POST:
        devs["from_WASTE"]["feasible"] = request.POST["feasible_waste_heat"]
	
    # If temperature is not high enough to heat the ectogrid, the waste heat is not used
    if devs["from_WASTE"]["waste_heat_temp"] < param["network_temp_hot"]:
        devs["from_WASTE"]["feasible"] = "False"
    
	# Waste cooling power
    devs["from_WASTE_cold"] = {"feasible": "False",
                               "waste_cold_temp": float(request.POST["waste_cold_temp"]),
                               "waste_cooling_power": float(request.POST["waste_cooling_power"]),
                               "price_waste_cold": float(request.POST["price_waste_cold"]),
                               "inv_var": 0,
    				           "life_time": 20,
    				           "cost_om": 0,
                               }
    
    if "feasible_waste_cold" in request.POST:
        devs["from_WASTE_cold"]["feasible"] = request.POST["feasible_waste_cold"]
	
    # If temperature is not low enough to cool the ectogrid, the waste cooling power is not used
    if devs["from_WASTE_cold"]["waste_cold_temp"] > param["network_temp_cold"]:
        devs["from_WASTE_cold"]["feasible"] = "False"
    
	# Photovoltaics
    devs["PV"] = {"feasible": "False",
				  "inv_var":    float(request.POST["PV_inv_var"]),
				  "eta":        float(request.POST["PV_eta"]),
				  "life_time":  float(request.POST["PV_life_time"]),
				  "cost_om":    float(request.POST["PV_cost_om"]),
				  "min_area":   float(request.POST["PV_min_area"]),
                  "max_area":   float(request.POST["PV_max_area"]),
				  "module_area":        1.7272,    # m2,
				  "nom_module_power":   360,  # W,
				  }

    if "feasible_PV" in request.POST:
        devs["PV"]["feasible"] = request.POST["feasible_PV"]
		
    devs["PV"]["PV_power_norm"] = calc_PV_power(param["GHI"], param["air_temp"], devs)
    devs["PV"]["max_cap"] = devs["PV"]["max_area"] / (devs["PV"]["module_area"] / devs["PV"]["nom_module_power"] * 1000) # kW_peak
    devs["PV"]["min_cap"] = devs["PV"]["min_area"] / (devs["PV"]["module_area"] / devs["PV"]["nom_module_power"] * 1000) # kW_peak
    
	# Gas boiler
    devs["BOI"] = {"feasible":  "False",
				   "inv_var":   float(request.POST["BOI_inv_var"]),
				   "eta_th":    float(request.POST["BOI_eta_th"]),
				   "life_time": float(request.POST["BOI_life_time"]),
				   "cost_om":   float(request.POST["BOI_cost_om"]),
				   "min_cap":   float(request.POST["BOI_min_cap"]),
                   "max_cap":   float(request.POST["BOI_max_cap"])
				   }
				   
    if "feasible_BOI" in request.POST:
        devs["BOI"]["feasible"] = request.POST["feasible_BOI"]
	
	# CHP
    devs["CHP"] = {"feasible":  "False",
				   "inv_var":   float(request.POST["CHP_inv_var"]),
				   "eta_el":    float(request.POST["CHP_eta_el"]),
				   "eta_th":    float(request.POST["CHP_eta_th"]),
				   "life_time": float(request.POST["CHP_life_time"]),
				   "cost_om":   float(request.POST["CHP_cost_om"]),
				   "min_cap":   float(request.POST["CHP_min_cap"]),
                   "max_cap":   float(request.POST["CHP_max_cap"])
				   }
				   
    if "feasible_CHP" in request.POST:
        devs["CHP"]["feasible"] = request.POST["feasible_CHP"]
	
	# Electric heater
    devs["EH"] = {"feasible":   "False",
				   "inv_var":   float(request.POST["EH_inv_var"]),
				   "eta_th":    float(request.POST["EH_eta_th"]),
				   "life_time": float(request.POST["EH_life_time"]),
				   "cost_om":   float(request.POST["EH_cost_om"]),
				   "min_cap":   float(request.POST["EH_min_cap"]),
                   "max_cap":   float(request.POST["EH_max_cap"])
				   }
				   
    if "feasible_EH" in request.POST:
        devs["EH"]["feasible"] = request.POST["feasible_EH"]
	
	# Ground source heat pump
    devs["GSHP"] = {"feasible": "False",
				   "inv_var":   float(request.POST["GSHP_inv_var"]),
				   "COP":       float(request.POST["GSHP_COP"]),
				   "life_time": float(request.POST["GSHP_life_time"]),
				   "cost_om":   float(request.POST["GSHP_cost_om"]),
				   "min_cap":   float(request.POST["GSHP_min_cap"]),
                   "max_cap":   float(request.POST["GSHP_max_cap"]),
				   }
				   
    if "feasible_GSHP" in request.POST:
        devs["GSHP"]["feasible"] = request.POST["feasible_GSHP"]
	
	# Compression chiller
    devs["CC"] = {"feasible":   "False",
				   "inv_var":   float(request.POST["CC_inv_var"]),
				   "COP":       float(request.POST["CC_COP"]),
				   "life_time": float(request.POST["CC_life_time"]),
				   "cost_om":   float(request.POST["CC_cost_om"]),
				   "min_cap":   float(request.POST["CC_min_cap"]),
                   "max_cap":   float(request.POST["CC_max_cap"]),
				   }
	
    if "feasible_CC" in request.POST:
        devs["CC"]["feasible"] = request.POST["feasible_CC"]
	
	# Absorption chiller
    devs["AC"] = {"feasible":   "False",
				   "inv_var":   float(request.POST["AC_inv_var"]),
				   "eta_th":    float(request.POST["AC_eta_th"]),
				   "life_time": float(request.POST["AC_life_time"]),
				   "cost_om":   float(request.POST["AC_cost_om"]),
				   "min_cap":   float(request.POST["AC_min_cap"]),
                   "max_cap":   float(request.POST["AC_max_cap"]),
				   }
	
    if "feasible_AC" in request.POST:
        devs["AC"]["feasible"] = request.POST["feasible_AC"]
			
	# Heat thermal energy storage
    devs["TES"] = {"feasible":  "False",
				   "inv_var":   float(request.POST["TES_inv_var"]),
				   "sto_loss":  float(request.POST["TES_sto_loss"]),
				   "life_time": float(request.POST["TES_life_time"]),
				   "cost_om":   float(request.POST["TES_cost_om"]),
				   "min_cap":   float(request.POST["TES_min_cap"]),
				   "max_cap":   float(request.POST["TES_max_cap"]),
				   "eta_ch":    1, 		# ---,              charging efficiency
				   "eta_dch":   1,		# ---,              discharging efficiency
				   "soc_init":  0.8,   	# ---,              maximum initial state of charge
				   }
				   
    if "feasible_TES" in request.POST:
        devs["TES"]["feasible"] = request.POST["feasible_TES"]
	
	# Cold thermal energy storage
    devs["CTES"] = {"feasible": "False",
        		    "inv_var":  float(request.POST["CTES_inv_var"]),
                    "sto_loss":     float(request.POST["CTES_sto_loss"]),
                    "life_time": float(request.POST["CTES_life_time"]),
                    "cost_om":  float(request.POST["CTES_cost_om"]),
                    "min_cap":  float(request.POST["CTES_min_cap"]),
		            "max_cap":  float(request.POST["CTES_max_cap"]),
		            "eta_ch":   1, 		# ---,              charging efficiency
		            "eta_dch":  1,		# ---,              discharging efficiency
		            "soc_init": 0.8,   	# ---,              maximum initial state of charge
                    }
    
    if "feasible_CTES" in request.POST:
        devs["CTES"]["feasible"] = request.POST["feasible_CTES"]
	
	# Battery
    devs["BAT"] = {"feasible":  "False",
				   "inv_var":   float(request.POST["BAT_inv_var"]),
				   "eta_ch":    float(request.POST["BAT_eta_ch"]),
				   "eta_dch":   float(request.POST["BAT_eta_ch"]),
				   "life_time": float(request.POST["BAT_life_time"]),
				   "cost_om":   float(request.POST["BAT_cost_om"]),
				   "min_cap":   float(request.POST["BAT_min_cap"]),
				   "max_cap":   float(request.POST["BAT_max_cap"]),
				   "sto_loss":  0,		# 1/h,              standby losses over one time step
				   "soc_init":  0.8,   	# ---,              maximum initial state of charge
				   }
				   
    if "feasible_BAT" in request.POST:
        devs["BAT"]["feasible"] = request.POST["feasible_BAT"]

	
	# District heating connection
    devs["from_DH"] = {"feasible":     "False",
					   "inv_var":      0.01,       # kEUR/MW_th,      variable investment
					   "max_cap":      1000000,    # kW_th,           maximum thermal capacity
					   "min_cap":      0,          # kW_th,           minimum thermal capacity              
					   "life_time":    50,         # a,               operation time
					   "cost_om":      0.01,       # ---,             annual operation and maintenance costs as share of investment
                       "price_DH":     float(request.POST["price_DH"])
					   }

    if "feasible_DH" in request.POST:
        devs["from_DH"]["feasible"] = request.POST["feasible_DH"]
					   
    devs["from_DC"] = {"feasible":     "False",
					   "inv_var":      0.01,        # kEUR/MW_th,      variable investment
					   "max_cap":      1000000,     # kW_th,           maximum thermal capacity
					   "min_cap":      0,           # kW_th,           minimum thermal capacity              
					   "life_time":    50,          # a,               operation time
					   "cost_om":      0.01,        # ---,             annual operation and maintenance costs as share of investment
                       "price_DC":     float(request.POST["price_DC"])
					   }
					   
    if "feasible_DC" in request.POST:
        devs["from_DC"]["feasible"] = request.POST["feasible_DC"]
					   
	
	################################################################
	# LOAD ECONOMIC PARAMETER
    
    param["interest_rate"] =        float(request.POST["interest_rate"]) / 100 # convert from percentage to number
    param["observation_time"] =     float(request.POST["observation_time"])
    param["other_invest_costs"] =   float(request.POST["other_invest_costs"])
    param["other_ann_costs"] =      float(request.POST["other_ann_costs"])
    param["price_gas"] =            float(request.POST["price_gas"])
    param["price_el"] =             float(request.POST["price_el"])
    
    if "feasible_feed_in" in request.POST:
        param["feasible_feed_in"] = request.POST["feasible_feed_in"]
    else:
        param["feasible_feed_in"] = "False"   
        
    param["revenue_feed_in"] =      float(request.POST["revenue_feed_in"])
    param["grid_CO2_emission"] =    float(request.POST["grid_CO2_emission"])
    param["gas_CO2_emission"] =     float(request.POST["gas_CO2_emission"])
    param["costs_earth_work"] =     float(request.POST["costs_earth_work"])
    param["costs_pipe"] =           float(request.POST["costs_pipe"])
	
	################################################################
	# INITIALIZE CALCULATION
	
    # Calculate annual investments
    devs, param = calc_annual_investment(devs, param)
    
	# Calculate COPs for HP and CC in buildings 
    
    if param["enable_use_fix_cops"] == True:
        param["COP_HP"] = param["COP_heat_pumps"] * np.ones(8760)
        param["COP_CC"] = param["COP_chillers"] * np.ones(8760)
		
    if param["enable_use_carnot_cops"] == True:
        param = calc_COP_carnot(param)

    print("==========================")
    print("COP_HP: " + str(param["COP_HP"]))
    print("COP_CC: " + str(param["COP_CC"]))
	
    # old version based on E.ON data: sup_temp = load_supply_temperature(param["air_temp"])
	# old version based on E.ON data: param["COP_HP"], param["COP_CC"] = load_COP(sup_temp, param)
	    
    return param, devs, nodes

#%% SUB-FUNCTIONS ##################################################

def calc_COP_carnot(param):
    param["COP_HP"] = param["carnot_efficiency"] * ((param["supply_temperature_heating"]+273)/(param["supply_temperature_heating"]-param["network_temp_hot"])) * np.ones(8760)
    param["COP_CC"] = param["carnot_efficiency"] * ((param["supply_temperature_cooling"]+273)/(param["network_temp_cold"]-param["supply_temperature_cooling"])) * np.ones(8760)
    
    print("carnot_efficiency: " + str(param["carnot_efficiency"]))
    print("supply_temperature_heating: " + str(param["supply_temperature_heating"]))
    print("network_temp_cold: " + str(param["network_temp_cold"]))
	
	
    return param


def calc_annual_investment(devs, param):
    """
    Calculation of total investment costs including replacements (based on VDI 2067-1, pages 16-17).

    Parameters
    ----------
    dev : dictionary
        technology parameter
    param : dictionary
        economic parameters

    Returns
    -------
    annualized fix and variable investment
    """

    observation_time = param["observation_time"]
    interest_rate = param["interest_rate"]
    q = 1 + param["interest_rate"]
    
    # Calculate capital recovery factor
    CRF = ((q**observation_time)*interest_rate)/((q**observation_time)-1)

    for device in devs.keys():

        # In this model:
        devs[device]["inv_fix"] = 0
        life_time = devs[device]["life_time"]
        inv_fix_init = devs[device]["inv_fix"]
        inv_var_init = devs[device]["inv_var"]
        inv_fix_repl = devs[device]["inv_fix"]
        inv_var_repl = devs[device]["inv_var"]

        # Number of required replacements
        n = int(math.floor(observation_time / life_time))

        # Inestment for replcaments
        invest_replacements = sum((q ** (-i * life_time)) for i in range(1, n+1))

        # Residual value of final replacement
        res_value = ((n+1) * life_time - observation_time) / life_time * (q ** (-observation_time))

        # Calculate annualized investments       
        if life_time > observation_time:
            devs[device]["ann_inv_fix"] = (inv_fix_init * (1 - res_value)) * CRF 
            devs[device]["ann_inv_var"] = (inv_var_init * (1 - res_value)) * CRF 
        else:
            devs[device]["ann_inv_fix"] = (inv_fix_init + inv_fix_repl * (invest_replacements - res_value)) * CRF
            devs[device]["ann_inv_var"] = (inv_var_init + inv_var_repl * (invest_replacements - res_value)) * CRF 
            
    # Convert other costs
    param["CRF"] = CRF
    param["ann_costs_earth_work"] = param["costs_earth_work"] * CRF 
    param["ann_costs_pipe"] = param["costs_pipe"] * CRF
    param["ann_other_invest_costs"] = param["other_invest_costs"] * CRF
    param["ann_inv_var_hp_bldgs"] = param["inv_var_hp_bldgs"] * CRF
    param["ann_inv_var_cc_bldgs"] = param["inv_var_cc_bldgs"] * CRF    
    param["ann_inv_var_eh_bldgs"] = param["inv_var_eh_bldgs"] * CRF
    param["ann_inv_var_boi_bldgs"] = param["inv_var_boi_bldgs"] * CRF
    param["ann_inv_var_frc_bldgs"] = param["inv_var_frc_bldgs"] * CRF	

    return devs, param

def calc_PV_power(GHI, air_temp, devs):
    
	PV_power = GHI * devs["PV"]["eta"] / 1000 # kW
	
	PV_power_norm = PV_power / (devs["PV"]["nom_module_power"] / devs["PV"]["module_area"] / 1000)
	return PV_power_norm

def calc_norm_demands(air_temp, heating_threshold, cooling_threshold):
    
	norm_heat_dem = np.zeros(8760)
	norm_cool_dem = np.zeros(8760)
    
	deg_day_heat = np.zeros(8760)
	deg_day_cool = np.zeros(8760)
    
	t_set = 20
    
	for t in range(8760):
		if air_temp[t] < heating_threshold:
			deg_day_heat[t] = t_set - air_temp[t]
		if air_temp[t] > cooling_threshold:
			deg_day_cool[t] = air_temp[t] - cooling_threshold
        
	# normalize
	deg_day_heat_sum = np.sum(deg_day_heat)
	deg_day_cool_sum = np.sum(deg_day_cool)
	for t in range(8760):
		norm_heat_dem[t] = deg_day_heat[t] / deg_day_heat_sum
		norm_cool_dem[t] = deg_day_cool[t] / deg_day_cool_sum
    
	return norm_heat_dem, norm_cool_dem

def load_supply_temperature(air_temp):

    # Temperature from heat pump to building energy system.
    # Determined via the correlation with air temperature, used by Swedish DH team and Tobias Blacha (see Ectogrid Early Assessment.xlsx)
    sup_temp =-(0.7471*air_temp)+54.75

    return sup_temp

def load_COP(sup_temp, param):
    
    # Based in manufacturer data from NIBE
	DT = 5
	T_cond = sup_temp
	T_evap = param["network_temp_hot"]
	HC_ber = 1.33 * ((-0.0021 * T_cond + 0.7919) * T_evap + (-0.0767 * T_cond + 27.156)) * ((1+(DT-8)*0.005))
	Pin_ber = (1.32 * (0.000805 * T_cond + 0.00105) * T_evap + (0.1171 * T_cond + 1.3472) * 1.42) * ((1+(DT-8)*0.005))
	COP_HP = HC_ber/Pin_ber
    
	T_cond = param["network_temp_cold"]
	T_evap = param["cold_supply_temp"]
	HC_ber = 1.33 * ((-0.0021 * T_cond + 0.7919) * T_evap + (-0.0767 * T_cond + 27.156)) * ((1+(DT-8)*0.005))
	Pin_ber = (1.32 * (0.000805 * T_cond + 0.00105) * T_evap + (0.1171 * T_cond + 1.3472) * 1.42) * ((1+(DT-8)*0.005))
	COP_CC = HC_ber/Pin_ber
    
	return COP_HP, COP_CC 

