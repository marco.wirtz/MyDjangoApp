# -*- coding: utf-8 -*-

"""

ECTOPLANNER SOFTWARE

Developed by:  E.ON Energy Research Center, 
               Institute for Energy Efficient Buildings and Indoor Climate, 
               RWTH Aachen University, 
               Germany

Author:        Marco Wirtz

Development period: 2018/2019

"""

import numpy as np
from myapp.help_functions import gurobi_optim
    
def design_balancing_unit(nodes, devs, param, time_steps):

    residual = {}
    residual["heat"] = np.zeros(8760)
    residual["cool"] = np.zeros(8760)

    # Calculate mass flow through balancing unit (inter-balancing): "> 0": flow from hot supply to cold return pipe 
    sum_residual_heat = sum(nodes[n]["res_heat_dem"] for n in range(len(nodes)))
    residual["power"] = sum(nodes[n]["power_HP"] + nodes[n]["power_EH"] + nodes[n]["power_CC"] for n in range(len(nodes)))  
        
    for t in time_steps:
        if sum_residual_heat[t] > 0:
            residual["heat"][t] = sum_residual_heat[t]
        else:
            residual["cool"][t] = (-1) * sum_residual_heat[t]

    try:
        import gurobipy as gp
        res_bal_unit, dec_var, optimization_successful = gurobi_optim.run_optim(devs, param, residual, time_steps)    
    except:
        from myapp.help_functions import get_test_result
        res_bal_unit, dec_var, optimization_successful = get_test_result.get_results(time_steps)    
    
        
    if optimization_successful:
        res_subsidies = get_subsidies(nodes, dec_var, devs, param, time_steps)
            
        res_bal_unit["residual"] = residual

        return res_bal_unit, res_subsidies, optimization_successful
    
    else:
        return {}, {}, optimization_successful
    
def get_subsidies(nodes, dec_var, devs, param, time_steps):
    
    power = dec_var["power"]
    heat =  dec_var["heat"]
    ch = dec_var["ch"]
    cap = dec_var["cap"]
    
    # Determine PV-grid, PV-selfconsumption, CHP-grid, CHP-selfconsumption
    power_PV_grid = np.zeros(8760)
    power_CHP_grid = np.zeros(8760)
    power_PV_self = np.zeros(8760)
    power_CHP_self = np.zeros(8760)
    for t in time_steps:
        if (power["PV"][t] + power["CHP"][t]) > 0:
            power_PV_grid[t] = power["to_grid"][t] * power["PV"][t] / (power["PV"][t] + power["CHP"][t])
            power_CHP_grid[t] = power["to_grid"][t] * power["CHP"][t] / (power["PV"][t] + power["CHP"][t])
            power_PV_self[t] = power["PV"][t] - power_PV_grid[t]
            power_CHP_self[t] = power["CHP"][t] - power_CHP_grid[t]
        
    total_power_PV_grid = sum(power_PV_grid[t] for t in time_steps)
    total_power_CHP_grid = sum(power_CHP_grid[t] for t in time_steps)
    total_power_PV_self = sum(power_PV_self[t] for t in time_steps)
    total_power_CHP_self = sum(power_CHP_self[t] for t in time_steps)
        
    share_CHP_grid = total_power_CHP_grid / sum(power["CHP"][t] for t in time_steps)
    share_CHP_self = 1- share_CHP_grid

    # CHP SUBSIDIES    
    full_load_hours = 60000
    KWKG_CHP = 0
    if cap["CHP"] <= 50:
        KWKG_CHP += full_load_hours * cap["CHP"] * (share_CHP_grid * 0.08 + share_CHP_self * 0.04)
    else:
        KWKG_CHP += full_load_hours * 50 * (share_CHP_grid * 0.08 + share_CHP_self * 0.04)
        if cap["CHP"] <= 100:
            KWKG_CHP += full_load_hours * (cap["CHP"]-50) * (share_CHP_grid * 0.06 + share_CHP_self * 0.03)
        else:
            KWKG_CHP += full_load_hours * 50 * (share_CHP_grid * 0.06 + share_CHP_self * 0.03)
            if cap["CHP"] <= 250:
                KWKG_CHP += full_load_hours * (cap["CHP"]-100) * (share_CHP_grid * 0.05 + share_CHP_self * 0.02)
            else:
                KWKG_CHP += full_load_hours * 150 * (share_CHP_grid * 0.05 + share_CHP_self * 0.02)
                if cap["CHP"] <= 2000:
                    KWKG_CHP += full_load_hours * (cap["CHP"]-250) * (share_CHP_grid * 0.044 + share_CHP_self * 0.015)
                else:
                    KWKG_CHP += full_load_hours * 1750 * (share_CHP_grid * 0.044 + share_CHP_self * 0.015)
    
    # PV SUBSIDIES
    EEG_PV = (total_power_PV_grid * 0.0793 + total_power_PV_self * 0.02652)

    # THERMAL ENERGY STORAGE SUBSIDIES
    c_w = 4.18      # kJ/(kg K)
    rho_w = 1000    # kg/m3
    delta_T = 50    # K
    TES_volume = cap["TES"] *1000 / (c_w * rho_w * delta_T)
    print("TES_volume: " + str(TES_volume))

    # Check if 50 % of the charging heat comes from CHP
    for t in time_steps: 
        heat_not_from_CHP = ch["TES"][t] - heat["CHP"][t]

    if heat_not_from_CHP > sum(ch["TES"][t] for t in time_steps):
        precondition_TES_enough_CHP_heat = False
        KWKG_TES = 0
    else:
        precondition_TES_enough_CHP_heat = True
        KWKG_TES = 0
        KWKG_TES = np.min([250 * TES_volume, 10000000, 0.3 * cap["TES"] * devs["TES"]["inv_var"]])
            
    # HEAT PUMPS
    BAFA_HP = 0
    BAFA_number_HP = 0 
    for n in range(len(nodes)):
        if nodes[n]["hp_capacity"] <= 100:
            BAFA_HP += nodes[n]["hp_capacity"] * 100  # 100 EUR / kW heat pump capacity
            BAFA_number_HP += 1 # counter for number of subsidied heat pump units
            
    # Network (100 EUR/m)
    network_subsidy = np.min([param["network_length"] * 1000 * 100, 
                              0.4 * param["network_length"] * 1000 * (param["costs_earth_work"] + param["costs_pipe"]), 
                              10000000])

    
    res_subsidies = {"KWKG_CHP": round(KWKG_CHP,2),
                     "total_power_CHP_grid": round(total_power_CHP_grid,0),
                     "total_power_CHP_self": round(total_power_CHP_self,0),
                     
                     "precondition_TES_enough_CHP_heat": precondition_TES_enough_CHP_heat,
                     "KWKG_TES": round(KWKG_TES,2),
                     
                     "EEG_PV": round(EEG_PV,2),
                     "total_power_PV_grid": round(total_power_PV_grid,0),
                     "total_power_PV_self": round(total_power_PV_self,0),
                     
                     "BAFA_HP": round(BAFA_HP,2),
                     "BAFA_number_HP": BAFA_number_HP,
                     
                     "network_subsidy": round(network_subsidy,0),
                     }
    
    res_subsidies["total"] = round(sum(res_subsidies[k] for k in ["KWKG_CHP", "KWKG_TES", "BAFA_HP", "network_subsidy"]) + res_subsidies["EEG_PV"] / param["CRF"],2)
    
    res_subsidies["ann_total"] = round(param["CRF"] * res_subsidies["total"],2)
    
    return res_subsidies