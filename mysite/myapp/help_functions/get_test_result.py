# -*- coding: utf-8 -*-

"""

ECTOPLANNER SOFTWARE

Developed by:  E.ON Energy Research Center, 
               Institute for Energy Efficient Buildings and Indoor Climate, 
               RWTH Aachen University, 
               Germany

Author:        Marco Wirtz

Development period: 2018/2019

"""

import numpy as np

def get_results(time_steps):
    
    # Write Gurobi object data to dictionary
    dec_var = {}
    dec_var["power"] = {"PV": {},
                        "CHP": {},
                        "to_grid": {},
                        "from_grid": {},
                        }
    for k in dec_var["power"].keys():
        for t in time_steps:
            dec_var["power"][k][t] = 50
        
    dec_var["heat"] = {"CHP": {}}
    for k in dec_var["heat"].keys():
        for t in time_steps:
            dec_var["heat"][k][t] = 50
    
    dec_var["ch"] = {"TES": {}}
    for k in dec_var["ch"].keys():
        for t in time_steps:
            dec_var["ch"][k][t] = 50
    
    dec_var["cap"] = {"CHP": 123.4,
                      "TES": 123.4,
                      }
    
    res_bal_unit = {}
    res_bal_unit["obj"] = {}  
    set_obj = ["tac"]      
    for k in set_obj:
        res_bal_unit["obj"][k] = 123456.78
		
    res_bal_unit["cap"] = {}       
    for k in ["BOI", "EH", "GSHP", "CHP", "AC", "CC", "TES", "CTES", "BAT", "from_DH", "from_DC", "from_WASTE", "from_WASTE_cold", "PV"]:
        res_bal_unit["cap"][k] = 123.4
			
    res_bal_unit["ann_total_bu_equip_cost"] = 123456.7
    res_bal_unit["total_bu_equip_cost"] = 123456.7
    res_bal_unit["total_bu_om_cost"] = 123456.7
    res_bal_unit["total_from_grid"] = 123456.7
    res_bal_unit["total_to_grid"] = 123456.7
    res_bal_unit["total_from_DH"] = 123456.7
    res_bal_unit["total_from_DC"] = 123456.7
    res_bal_unit["total_from_WASTE"] = 123456.7
    res_bal_unit["total_from_WASTE_cold"] = 123456.7
    res_bal_unit["total_gas"] = 123456.7
    res_bal_unit["total_PV_power"] = 123456.7
    res_bal_unit["total_PV_area"] = 12345.6
    res_bal_unit["total_PV_power_curtailed"] = 123456.7
        
    optimization_successful = True
    
    
    return res_bal_unit, dec_var, optimization_successful