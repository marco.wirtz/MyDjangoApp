# -*- coding: utf-8 -*-

"""

ECTOPLANNER SOFTWARE

Developed by:  E.ON Energy Research Center, 
               Institute for Energy Efficient Buildings and Indoor Climate, 
               RWTH Aachen University, 
               Germany

Author:        Marco Wirtz

Development period: 2018/2019

"""

import numpy as np
import matplotlib.pyplot as plt

def collect_results(nodes, param, devs, res_bal_unit, time_steps):
    
    #%% Calculate Demand Overlap Coefficients and Diversity index
    # Calculate DOC for building demands
    DOC_dem = calc_DOC_bldg_demands(nodes, time_steps)
    
    # Calculate DOC internal balancing
    nodes, DOC_int_ges = calc_DOC_internal(nodes, param, time_steps)
    
    # Calculate DOC for network balancing
    DOC_netw = calc_DOC_network(nodes, time_steps)
    
    # Calculate diversity index
    div_index = calc_diversity_index(nodes)
    
    res_intra_bldg_bal = {"total_bldg_hp_cap": round(sum(nodes[n]["hp_capacity"] for n in range(len(nodes))),1),
                          "total_bldg_eh_cap": round(sum(nodes[n]["eh_capacity"] for n in range(len(nodes))),1),
                          "total_bldg_boi_cap": round(sum(nodes[n]["boi_capacity"] for n in range(len(nodes))),1),
                          "total_bldg_cc_cap": round(sum(nodes[n]["cc_capacity"] for n in range(len(nodes))),1),
                          "total_bldg_hp_cost": sum(nodes[n]["hp_costs"] for n in range(len(nodes))),      
                          "total_bldg_eh_cost": sum(nodes[n]["eh_costs"] for n in range(len(nodes))),      
                          "total_bldg_boi_cost": sum(nodes[n]["boi_costs"] for n in range(len(nodes))),      
                          "total_bldg_cc_cost": sum(nodes[n]["cc_costs"] for n in range(len(nodes))),
                          "DOC_int_ges": round(DOC_int_ges,2),
                          "DOC_dem": round(DOC_dem,2),
                          "div_index": div_index,
                          "total_bldg_equip_cost": round(sum(nodes[n]["hp_costs"] + nodes[n]["eh_costs"] + nodes[n]["boi_costs"] 
                                                   + nodes[n]["cc_costs"] for n in range(len(nodes))), 2),
                          "ann_total_bldg_equip_cost": round(sum(nodes[n]["hp_ann_costs"] + nodes[n]["eh_ann_costs"] + nodes[n]["boi_ann_costs"] 
                                                   + nodes[n]["cc_ann_costs"] for n in range(len(nodes))), 2),
                          "gas_demand_bldgs": round(sum(sum(nodes[n]["gas_BOI"][t] for t in time_steps) for n in range(len(nodes))), 2),
                          "mean_COP_HP": round(np.mean(param["COP_HP"]), 2), 
                          "mean_COP_CC": round(np.mean(param["COP_CC"]), 2),
                          }


    for n in range(len(nodes)):
        nodes[n]["total_heat"] = round(sum(nodes[n]["heat"][t] for t in time_steps),0)
        nodes[n]["total_cool"] = round(sum(nodes[n]["cool"][t] for t in time_steps),0)
    
    # Collect results for html-rendering
    res_bldg_bal = {"sum_power_dem_HP": round(sum(sum(nodes[n]["power_HP"][t] for n in range(len(nodes))) for t in time_steps),1),
                    "sum_power_dem_EH": round(sum(sum(nodes[n]["power_EH"][t] for n in range(len(nodes))) for t in time_steps),1),
                    "sum_power_dem_CC": round(sum(sum(nodes[n]["power_CC"][t] for n in range(len(nodes))) for t in time_steps),1),
                    "sum_power_dem_bldgs": round(sum(res_bal_unit["residual"]["power"][t] for t in time_steps),1),
                    "sum_residual_heat": round(sum(res_bal_unit["residual"]["heat"][t] for t in time_steps),1),
                    "sum_residual_cool": round(sum(res_bal_unit["residual"]["cool"][t] for t in time_steps),1),
                    "total_heat_demand": round(sum(sum(nodes[n]["heat"][t] for n in range(len(nodes))) for t in time_steps),1),
                    "total_cool_demand": round(sum(sum(nodes[n]["cool"][t] for n in range(len(nodes))) for t in time_steps),1),
                    "heat_demand": sum(nodes[n]["heat"] for n in range(len(nodes))),
                    "cool_demand": sum(nodes[n]["cool"] for n in range(len(nodes))),
                    "DOC_netw": round(DOC_netw,1),
                    }
    
    # Calculate main results
    main_results = {}
    main_results["ann_piping_cost"] = round(param["network_length"] * 1000 * (param["ann_costs_earth_work"] + param["ann_costs_pipe"]),0)
    main_results["piping_cost"] = round(param["network_length"] * 1000 * (param["costs_earth_work"] + param["costs_pipe"]),0)

    main_results["total_costs"] = round(res_bal_unit["obj"]["tac"] 
                                        + res_intra_bldg_bal["ann_total_bldg_equip_cost"] 
                                        + param["ann_other_invest_costs"] 
                                        + param["other_ann_costs"] 
                                        + res_intra_bldg_bal["gas_demand_bldgs"] * param["price_gas"]
                                        + main_results["ann_piping_cost"], 2)
    
    main_results["other_invest_costs"] = param["other_invest_costs"]
    main_results["ann_other_invest_costs"] = param["ann_other_invest_costs"]
    main_results["other_ann_costs"] = param["other_ann_costs"]
    main_results["spec_total_costs"] = round(main_results["total_costs"] / (res_bldg_bal["total_heat_demand"] + res_bldg_bal["total_cool_demand"]), 3)

    main_results["total_gas_demand"] = res_bal_unit["total_gas"] + res_intra_bldg_bal["gas_demand_bldgs"]
    main_results["total_gas_costs"] = round(main_results["total_gas_demand"] * param["price_gas"], 2)
    main_results["total_power_from_grid"] = res_bal_unit["total_from_grid"]
    main_results["total_power_from_grid_costs"] = round(main_results["total_power_from_grid"] * param["price_el"], 2)
    main_results["total_power_to_grid"] = res_bal_unit["total_to_grid"] # + PV
    main_results["total_power_to_grid_revenue"] = round(main_results["total_power_to_grid"] * param["revenue_feed_in"], 2)
    main_results["total_from_DH_costs"] = round(res_bal_unit["total_from_DH"] * devs["from_DH"]["price_DH"], 2)
    main_results["total_from_DC_costs"] = round(res_bal_unit["total_from_DC"] * devs["from_DC"]["price_DC"], 2)
    main_results["total_waste_heat_costs"] = round(res_bal_unit["total_from_WASTE"] * devs["from_WASTE"]["price_waste_heat"], 2)
    main_results["total_waste_cold_costs"] = round(res_bal_unit["total_from_WASTE_cold"] * devs["from_WASTE_cold"]["price_waste_cold"], 2)
    main_results["CO2"] = round(main_results["total_gas_demand"] * param["gas_CO2_emission"] 
                		+ main_results["total_power_from_grid"] * param["grid_CO2_emission"])
    main_results["CRF"] = round(param["CRF"], 3)
    
    # For development: Double-check sum of costs
#    main_results["CHECK_SUM"] = (# Investment costs
#                                 res_intra_bldg_bal["ann_total_bldg_equip_cost"] 
#                               + res_bal_unit["ann_total_bu_equip_cost"]
#                               + main_results["ann_piping_cost"]
#                               + main_results["ann_other_invest_costs"] 
#                               # Operation                                          
#                               + main_results["total_gas_costs"]
#                               + main_results["total_power_from_grid_costs"] 
#                               - main_results["total_power_to_grid_revenue"] 
#                               + main_results["total_from_DH_costs"] 
#                               + main_results["total_from_DC_costs"] 
#                               + main_results["total_waste_heat_costs"] 
#                               + main_results["total_waste_cold_costs"] 
#                               + res_bal_unit["total_bu_om_cost"] 
#                               + main_results["other_ann_costs"]
#                               )
    
    main_results["primary_energy_factor"] = round((main_results["total_gas_demand"] * 1.1 + (main_results["total_power_from_grid"] - 
                                            main_results["total_power_to_grid"]) * 1.8 + res_bal_unit["total_from_DH"] * 0.7 +
                                            res_bal_unit["total_from_DC"] * 0.7) / (res_bldg_bal["total_heat_demand"] + 
                                            res_bldg_bal["total_cool_demand"]), 2)
    
    main_results["report_text"] = get_result_report(DOC_dem, DOC_int_ges, DOC_netw, main_results["primary_energy_factor"])
    
    res_nodes = {}
    for n in range(len(nodes)):
        res_nodes[n] = {}
        for k in ["hp_capacity", "eh_capacity", "eh_costs", "boi_capacity",  "cc_capacity"]:
            res_nodes[n][k] = round(nodes[n][k],1)
        for k in ["hp_costs", "eh_costs", "boi_costs", "cc_costs", "DOC_int"]:
            res_nodes[n][k] = round(nodes[n][k],2)
        for k in ["total_heat", "total_cool", "total_power_dem", "total_gas_dem"]:
            res_nodes[n][k] = round(nodes[n][k],0)  
    
    
    # Create plots
    create_plots(nodes, res_bldg_bal, res_bal_unit)
    
    return main_results, res_intra_bldg_bal, res_bldg_bal, nodes, res_nodes 
    
    
def get_result_report(DOC_dem, DOC_int_ges, DOC_netw, PEF):
    
    # Waste heat utilization can be integrated in DOC recommendation
    if DOC_dem > 0.6:
        DOC_dem_fit = "fit very well to each other. Based on the thermal demands, an ectogrid is perfectly suited for this district."
    elif DOC_dem > 0.4:
        DOC_dem_fit = "fit well to each other. Based on the thermal demands, an ectogrid is well suited for this district."
    else:
        DOC_dem_fit = ("do not match to each other. Either heating or cooling demands predominate strongly in this district. It should be checked " +  
                      "if further buildings with complemental thermal demands or waste heat sources can be connected to the network.")
                      
    report_text = "Heating and cooling demands of the district " + DOC_dem_fit
    report_text = report_text + " The primary energy factor is " + str(PEF) + "."
    
    return report_text

def calc_DOC_network(nodes, time_steps):
    residual = {}
    for n in range(len(nodes)):
        residual[n] = {}
        residual[n]["heat"] = np.zeros(8760)
        residual[n]["cool"] = np.zeros(8760)
        for t in time_steps:
            if nodes[n]["res_heat_dem"][t] > 0:
                residual[n]["heat"][t] = nodes[n]["res_heat_dem"][t]
            else:
                residual[n]["cool"][t] = (-1) * nodes[n]["res_heat_dem"][t]
                
    residual_heat_all_nodes = np.zeros(8760)
    residual_cool_all_nodes = np.zeros(8760)
    for t in time_steps:
        residual_heat_all_nodes[t] = sum(residual[n]["heat"][t] for n in nodes)
        residual_cool_all_nodes[t] = sum(residual[n]["cool"][t] for n in nodes)

    min_all_nodes = np.array([np.min([residual_heat_all_nodes[t], residual_cool_all_nodes[t]]) for t in time_steps])
    DOC_netw = 2 * sum(min_all_nodes[t] for t in time_steps) / sum(residual_heat_all_nodes[t] + residual_cool_all_nodes[t] for t in time_steps)

    return DOC_netw


def calc_DOC_bldg_demands(nodes, time_steps):
    # Calculate DOC for building demands
    heat_all_nodes = sum(nodes[n]["heat"] for n in range(len(nodes)))
    cool_all_nodes = sum(nodes[n]["cool"] for n in range(len(nodes)))
    min_all_nodes = np.array([np.min([heat_all_nodes[t], cool_all_nodes[t]]) for t in time_steps])
    print(min_all_nodes)
    DOC_dem = 2 * sum(min_all_nodes[t] for t in time_steps) / sum(heat_all_nodes[t] + cool_all_nodes[t] for t in time_steps)
    
    return DOC_dem


def calc_DOC_internal(nodes, param, time_steps):    # Calculate DOC internal balancing
    min_dem = {}
    for n in range(len(nodes)):
        min_dem[n] = np.array([np.min([nodes[n]["BES_heat_dem"][t], nodes[n]["BES_cool_dem"][t]]) for t in time_steps])
        DOC_int = 2 * sum(min_dem[n][t] for t in time_steps) / sum((nodes[n]["BES_heat_dem"][t] + nodes[n]["BES_cool_dem"][t]) for t in time_steps)
        nodes[n]["DOC_int"] = DOC_int
    DOC_int_ges = sum(sum(min_dem[n][t] for t in time_steps) for n in nodes) / sum(sum(nodes[n]["BES_heat_dem"][t] + nodes[n]["BES_cool_dem"][t] for t in time_steps) for n in nodes)
    
    return nodes, DOC_int_ges


def calc_diversity_index(nodes):
	sum_heat_dem = sum(nodes[n]["heat"] for n in range(len(nodes)))  
	sum_cool_dem = sum(nodes[n]["cool"] for n in range(len(nodes)))
    
	div_index_sum = 0
	counts = 0
	for t in range(8760):
		if sum_cool_dem[t]+sum_heat_dem[t] != 0:
			div_index_sum += (2 * (1-((sum_cool_dem[t]**2+sum_heat_dem[t]**2)/((sum_cool_dem[t]+sum_heat_dem[t])**2))))
			counts += 1
            
	div_index = div_index_sum / counts
    
	return round(div_index, 2)

def create_plots(nodes, res_bldg_bal, res_bal_unit):
    
    # For development: Create plots for each building
    if 1 == 1:
        for n in range(len(nodes)):
                # Create plot
            plt.plot(nodes[n]["heat"], color="red", label="Heating demand")
            plt.plot(nodes[n]["cool"], color="blue", label="Cooling demand")
            plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=5, mode="expand", borderaxespad=0.)
            # Save plot
            file_name = "myapp//static//Demands_bldg_" + str(n)
            plt.savefig(file_name + ".png", dpi = 200, format = "png", bbox_inches="tight", pad_inches=0.1)
            plt.clf()
            plt.close()

    # Demand plot
    plt.plot(res_bldg_bal["heat_demand"], color="red", label="Heating demand")
    plt.plot(res_bldg_bal["cool_demand"], color="blue", label="Cooling demand")
    plt.xlabel = "Year profile"
    plt.ylabel = "Thermal building demand [kW]"
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=5, mode="expand", borderaxespad=0.)
    # Save plot
    file_name = "myapp//static//Total_demands"
    plt.savefig(file_name + ".png", dpi = 200, format = "png", bbox_inches="tight", pad_inches=0.1)
    plt.clf()
    plt.close()
    
    # Residual demand plot
    plt.plot(res_bal_unit["residual"]["heat"], color="red", label="Residual heating demand")
    plt.plot(res_bal_unit["residual"]["cool"], color="blue", label="Residual cooling demand")
    plt.xlabel = "Year profile"
    plt.ylabel = "Thermal residual demand [kW]"
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=5, mode="expand", borderaxespad=0.)
    # Save plot
    file_name = "myapp//static//Total_residual_demands"
    plt.savefig(file_name + ".png", dpi = 200, format = "png", bbox_inches="tight", pad_inches=0.1)
    plt.clf()
    plt.close()