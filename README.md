# My first Django app

Start app with

```python manage.py runserver```

and open 

```http://127.0.0.1:8000/myapp/```

or directly:

```http://127.0.0.1:8000/myapp/calc/```
